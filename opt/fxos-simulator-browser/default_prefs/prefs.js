/* DISABLE UPDATES */
user_pref("app.update.auto", false);
user_pref("app.update.channel", "default");
user_pref("app.update.enabled", false);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 0);

/* DISABLE SIGNATURE VERIFICATION WHEN INSTALLING EXTENSIONS (UNSECURE BUT NEEDED FOR SIMULATORS!) */
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("xpinstall.signatures.required", false);

/* ENABLE WEBIDE */
user_pref("devtools.webide.autoinstallADBHelper", false);
user_pref("devtools.webide.autoinstallFxdtAdapters", false);
user_pref("devtools.webide.widget.enabled", true);
user_pref("devtools.webide.addonsURL_cache", "{\n  \"stable\": [\"1.3\", \"1.4\", \"2.0\", \"2.1\", \"2.2\"],\n  \"unstable\": [\"2.6\", \"2.6_tv\"]\n}\n");
user_pref("devtools.webide.autoinstallADBHelper", false);
user_pref("devtools.webide.autoinstallFxdtAdapters", false);
user_pref("devtools.webide.widget.enabled", true);

/* TELEMETRY */
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("devtools.telemetry.tools.opened.version", "{\"DEVTOOLS_WEBIDE_OPENED_PER_USER_FLAG\":\"45.9.0\"}");

/* DISABLE DEFAULT BROWSER CHECK */
user_pref("browser.shell.checkDefaultBrowser", false);

/* DOM */
user_pref("dom.apps.reset-permissions", true);
user_pref("dom.mozApps.used", true);

/* DISABLE EXPERIMENTS */
user_pref("experiments.activeExperiment", false);

/* EXTENSIONS */
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.bootstrappedAddons", "{\"adbhelper@mozilla.org\":{\"version\":\"0.12.1\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/adbhelper@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":false},\"fxdevtools-adapters@mozilla.org\":{\"version\":\"0.3.8\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxdevtools-adapters@mozilla.org\",\"multiprocessCompatible\":true,\"runInSafeMode\":false},\"loop@mozilla.org\":{\"version\":\"1.1.14\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/browser/features/loop@mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":true},\"adb@mozilla.org\":{\"version\":\"0.0.2\",\"type\":\"webextension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/adb@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":false},\"fxos_3_0_simulator@mozilla.org\":{\"version\":\"3.0.20150526.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_3_0_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_2_6_tv_simulator@mozilla.org\":{\"version\":\"2.6.20151125005937\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_6_tv_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_2_2_simulator@mozilla.org\":{\"version\":\"2.2.20150330.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_2_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_2_0_simulator@mozilla.org\":{\"version\":\"2.0.20140918.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_0_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_2_1_simulator@mozilla.org\":{\"version\":\"2.1.20141223.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_1_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_1_2_simulator@mozilla.org\":{\"version\":\"6.0pre9.20140401\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_2_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_1_4_simulator@mozilla.org\":{\"version\":\"1.4.20140506.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_4_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_1_5_simulator@mozilla.org\":{\"version\":\"1.5.20140402\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_5_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_2_6_simulator@mozilla.org\":{\"version\":\"2.6.20151123131605\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_6_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false},\"fxos_1_3_simulator@mozilla.org\":{\"version\":\"7.0pre9.20140401.1-signed\",\"type\":\"extension\",\"descriptor\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_3_simulator@mozilla.org\",\"multiprocessCompatible\":false,\"runInSafeMode\":false}}");
user_pref("extensions.databaseSchema", 17);
user_pref("extensions.e10sBlockedByAddons", true);
user_pref("extensions.enabledAddons", "%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:45.9.0");
user_pref("extensions.fxdevtools-adapters@mozilla.org.sdk.baseURI", "resource://fxdevtools-adapters-at-mozilla-dot-org/");
user_pref("extensions.fxdevtools-adapters@mozilla.org.sdk.domain", "fxdevtools-adapters-at-mozilla-dot-org");
user_pref("extensions.fxdevtools-adapters@mozilla.org.sdk.load.reason", "startup");
user_pref("extensions.fxdevtools-adapters@mozilla.org.sdk.rootURI", "file:///opt/fxos-simulator-browser/profiles/default_profile/extensions/fxdevtools-adapters@mozilla.org/");
user_pref("extensions.fxdevtools-adapters@mozilla.org.sdk.version", "0.3.8");
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppVersion", "45.9.0");
user_pref("extensions.lastPlatformVersion", "45.9.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.shownSelectionUI", true);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webextensions.uuids", "{\"adb@mozilla.org\":\"c883ea72-200a-4186-a2be-941b19134908\"}");
user_pref("extensions.xpiState", "{\"app-profile\":{\"fxos_2_6_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_6_simulator@mozilla.org\",\"e\":true,\"v\":\"2.6.20151123131605\",\"st\":1702637566000,\"mt\":1702637565000},\"fxos_2_6_tv_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_6_tv_simulator@mozilla.org\",\"e\":true,\"v\":\"2.6.20151125005937\",\"st\":1702637381000,\"mt\":1702637381000},\"fxos_2_2_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_2_simulator@mozilla.org\",\"e\":true,\"v\":\"2.2.20150330.1-signed\",\"st\":1702637437000,\"mt\":1702637436000},\"adbhelper@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/adbhelper@mozilla.org.xpi\",\"e\":true,\"v\":\"0.12.1\",\"st\":1702636746000},\"fxdevtools-adapters@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxdevtools-adapters@mozilla.org\",\"e\":true,\"v\":\"0.3.8\",\"st\":1702636747000,\"mt\":1702636746000},\"fxos_2_0_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_0_simulator@mozilla.org\",\"e\":true,\"v\":\"2.0.20140918.1-signed\",\"st\":1702637453000,\"mt\":1702637452000},\"fxos_1_4_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_4_simulator@mozilla.org\",\"e\":true,\"v\":\"1.4.20140506.1-signed\",\"st\":1702637554000,\"mt\":1702637549000},\"fxos_2_1_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_2_1_simulator@mozilla.org\",\"e\":true,\"v\":\"2.1.20141223.1-signed\",\"st\":1702637536000,\"mt\":1702637535000},\"adb@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/adb@mozilla.org.xpi\",\"e\":true,\"v\":\"0.0.2\",\"st\":1702637252000},\"fxos_3_0_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_3_0_simulator@mozilla.org\",\"e\":true,\"v\":\"3.0.20150526.1-signed\",\"st\":1702637314000,\"mt\":1702637313000},\"fxos_1_2_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_2_simulator@mozilla.org\",\"e\":true,\"v\":\"6.0pre9.20140401\",\"st\":1702637540000,\"mt\":1702637534000},\"fxos_1_5_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_5_simulator@mozilla.org\",\"e\":true,\"v\":\"1.5.20140402\",\"st\":1702637555000,\"mt\":1702637552000},\"fxos_1_3_simulator@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/profiles/default_profile/extensions/fxos_1_3_simulator@mozilla.org\",\"e\":true,\"v\":\"7.0pre9.20140401.1-signed\",\"st\":1702637583000,\"mt\":1702637572000}},\"app-system-defaults\":{\"loop@mozilla.org\":{\"d\":\"/opt/fxos-simulator-browser/browser/features/loop@mozilla.org.xpi\",\"e\":true,\"v\":\"1.1.14\",\"st\":1702632286000}},\"app-global\":{\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"/opt/fxos-simulator-browser/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}.xpi\",\"e\":true,\"v\":\"45.9.0\",\"st\":1702632286000}}}");