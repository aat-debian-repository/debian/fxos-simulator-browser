user_pref('browser.manifestURL', "app://system.gaiamobile.org/manifest.webapp");
user_pref('b2g.neterror.url', "app://system.gaiamobile.org/net_error.html");
user_pref('browser.homescreenURL', "app://system.gaiamobile.org/index.html");
user_pref('network.http.max-connections-per-server', 15);
user_pref('dom.mozInputMethod.enabled', true);
user_pref('layout.css.sticky.enabled', true);
user_pref('ril.debugging.enabled', false);
user_pref('dom.mms.version', 17);
user_pref('b2g.wifi.allow_unsafe_wpa_eap', true);
user_pref('network.dns.localDomains', "gaiamobile.org,browser.gaiamobile.org,clock.gaiamobile.org,keyboard.gaiamobile.org,setringtone.gaiamobile.org,ringtones.gaiamobile.org,fl.gaiamobile.org,fm.gaiamobile.org,pdfjs.gaiamobile.org,communications.gaiamobile.org,calendar.gaiamobile.org,homescreen.gaiamobile.org,music.gaiamobile.org,video.gaiamobile.org,email.gaiamobile.org,bluetooth.gaiamobile.org,system.gaiamobile.org,settings.gaiamobile.org,wallpaper.gaiamobile.org,costcontrol.gaiamobile.org,camera.gaiamobile.org,wappush.gaiamobile.org,gallery.gaiamobile.org,search.gaiamobile.org,sms.gaiamobile.org,marketplace.firefox.com.gaiamobile.org");

pref("geo.gps.supl_server", "supl.izatcloud.net");
pref("geo.gps.supl_port", 22024);
pref("dom.payment.provider.0.name", "firefoxmarket");
pref("dom.payment.provider.0.description", "marketplace.firefox.com");
pref("dom.payment.provider.0.uri", "https://marketplace.firefox.com/mozpay/?req=");
pref("dom.payment.provider.0.type", "mozilla/payments/pay/v1");
pref("dom.payment.provider.0.requestMethod", "GET");

user_pref("devtools.debugger.prompt-connection", false);
user_pref("devtools.debugger.forbid-certified-apps", false);
